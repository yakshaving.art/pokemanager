package slack

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
)

type MessageParser struct {
	MyID string
	RTM  *slack.RTM
}

func newMessageParser(rtm *slack.RTM) MessageParser {
	return MessageParser{
		RTM: rtm,
	}
}

func (mp *MessageParser) ShouldCare(event *slack.MessageEvent) bool {
	// if mp.isPrivateMessage(event.Channel) {
	// 	action, pokeID, err := mp.getAction(event)
	// 	if err != nil {
	// 		// m := &slack.OutgoingMessage{
	// 		// 	Channel: event.User,
	// 		// 	Text:    "Please reply \"ack\" or \"pass\"",
	// 		// }
	// 		// mp.RTM.SendMessage(m)
	// 		return IncomingMessage{}, fmt.Errorf("syntax error")
	// 	}
	// 	return IncomingMessage{
	// 		Action:           action,
	// 		AuthorID:         event.User,
	// 		Channel:          event.Channel,
	// 		IsPrivateMessage: true,
	// 		PokeID:           pokeID,
	// 		Text:             event.Text,
	// 	}, nil
	// } else {
	return mp.isMention(event.Text)
}

func (mp *MessageParser) isPrivateMessage(channel string) bool {
	return strings.HasPrefix(channel, "D")
}

// detChannelName returns a channel name given an ID
func (mp *MessageParser) getChannelName(channelID string) string {
	if mp.isPrivateMessage(channelID) {
		return "IM"
	}

	ch, err := mp.RTM.GetChannelInfo(channelID)
	if err != nil {
		logrus.Errorf("could not find channel with id %s: %s", channelID, err)
		return ""
	}
	return ch.Name
}

func (mp *MessageParser) isMyOwnMessage(event *slack.MessageEvent) bool {
	return event.User == mp.MyID
}

func (mp *MessageParser) getAction(event *slack.MessageEvent) (string, string, error) {
	if !mp.isPrivateMessage(event.Channel) {
		return "", "", nil
	}
	if strings.HasPrefix(event.Text, "ack") ||
		strings.HasPrefix(event.Text, "pass") {
		parts := strings.Split(event.Text, " ")
		if len(parts) == 2 {
			return parts[0], parts[1], nil
		}
	}
	return "", "", fmt.Errorf("invalid action")
}

// inMention checks if my own handle is contained in a message
func (mp *MessageParser) isMention(msg string) bool {
	return strings.Contains(msg, fmt.Sprintf("<@%s>", mp.MyID))
}
