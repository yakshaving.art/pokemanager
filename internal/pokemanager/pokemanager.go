package pokemanager

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/slack/pokemanager/internal/config"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/slack"
)

// Pokemanager contains the settings for pokemanager
// type Pokemanager struct {
// 	conf config.Config
// }

// type Poke struct {
// 	Poke      slack.Poke
// 	Usernames []string
// }

// type ActivePokes struct {
// 	Pokes map[string]Poke
// }

// func (p ActivePokes) contains(msg slack.IncomingMessage) string {
// 	for _, a := range p.Pokes {
// 		if a.Poke.Message == msg.Text &&
// 			a.Poke.Channel == msg.Channel &&
// 			a.Poke.AuthorID == msg.AuthorID {
// 			return a.Poke.PokeID
// 		}
// 	}
// 	return ""
// }

// Run listens for incoming messages from Slack and escalate them
func Run(conf config.Config, slackClient *slack.Client) {
	// activePokes := make(map[string]Poke)
	for {
		message := <-slackClient.IncomingMessages
		if message.IsPrivateMessage {
			// TODO: Reject private messages unless there is an escalation going on
			handleAction(message)
			// TODO: Remove poke from activePokes
			continue
		}
		mentionedUsers := MentionFor(conf, message)
		if len(mentionedUsers) == 0 {
			continue
		}
		logrus.Debugf("New message for -> %+v\n", mentionedUsers)

		// FIXME: This is terrible and should be refactored as soon as I sort
		// out the data model
		// if activePokes.contains(message) != "" {
		// 	logrus.Debug("Already seen. Skipping.")
		// 	continue
		// }

		// timeout := make(chan bool)
		// pokeID := generatePokeID()
		// poke := slack.Poke{
		// 	AuthorID:       message.AuthorID,
		// 	Channel:        message.Channel,
		// 	Message:        message.Text,
		// 	TimeoutSeconds: 5,
		// 	Timeout:        timeout,
		// }
		// // TODO: Handle the escalations from here
		// activePokes[pokeID] = Poke{
		// 	Poke:      poke,
		// 	Usernames: []string{message.AuthorID},
		// }
		// slackClient.PokeUser(message.AuthorID, pokeID, poke)
		// logrus.Debugf("Active pokes:\n")
		// for i, p := range activePokes {
		// 	logrus.Debugf("%s -> %+v\n", i, p)
		// }
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func generatePokeID() string {
	var chars = "0123456789abcdefghijklmnopqrstuvwxyz"
	b := make([]byte, 3)
	for i := range b {
		b[i] = chars[rand.Intn(len(chars))]
	}
	return string(b)
}

func handleAction(message slack.IncomingMessage) {
	// TODO: Check that pokeID is an active escalation
	switch {
	case message.Action == "ack":
		logrus.Infof("Acknowledged %s\n", message.PokeID)
		// TODO: Handle ack
	case message.Action == "pass":
		logrus.Infof("Passed %s\n", message.PokeID)
		// TODO: Handle pass
	default:
		logrus.Errorf("I've no idea how we got here: action: %+v", message)
	}
}

func MentionFor(conf config.Config, msg slack.IncomingMessage) []string {
	mentionedUsers := []string{}
	for user := range conf.Escalations {
		if strings.Contains(msg.Text, fmt.Sprintf("<@%s>", user)) {
			mentionedUsers = append(mentionedUsers, user)
		}
	}
	return mentionedUsers
}
