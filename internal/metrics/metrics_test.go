package metrics_test

import (
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/metrics"
)

func TestMetricsAreRegistered(t *testing.T) {
	a := assert.New(t)
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.BootTime),
		"unix timestamp of when the service was started")
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.BuildInfo),
		"build information")
}
