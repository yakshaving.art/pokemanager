module gitlab.com/yakshaving.art/slack/pokemanager

go 1.15

require (
	github.com/prometheus/client_golang v1.8.0
	github.com/sirupsen/logrus v1.7.0
	github.com/slack-go/slack v0.7.2
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.4.0
)
